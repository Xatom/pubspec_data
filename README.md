Converts the pubspec file to a Dart file in order to access information like version during runtime.

# Installation

Add the required packages to your `pubspec.yaml`:

```yaml
dependencies:
  pubspec_data: any
dev_dependencies:
  build_runner: any
  pubspec_data_generator: any
```

# Usage

Start the build runner:

```
pub run build_runner build
```

A `pubspec.g.dart` file will be created based on your `pubspec.yaml`:

```dart
import 'pubspec.g.dart' as pubspec;

void main() {
  print('Name: ${pubspec.name}');
  print('Version: ${pubspec.version}');
}
```

# Important note ⚠️

At the moment, it's only possible for generators to write to the same directory as the source file, unless the target path is hard-coded.
Because `pubspec.yaml` is usually in the root of the project and code is in `lib/src`, the target path has to be hard-coded for this package.
The target file will always be written to `lib/src/pubspec.g.dart` due to this issue, but the source path can be adjusted if desired.
