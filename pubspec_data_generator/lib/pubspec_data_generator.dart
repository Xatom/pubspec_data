// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of pubspec_data.
//
// Pubspec_data is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pubspec_data is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with pubspec_data.  If not, see <https://www.gnu.org/licenses/>.

import 'package:build/build.dart';

import 'src/pubspec_data_builder.dart';

Builder pubspecDataGeneratorFactory(BuilderOptions options) =>
    PubspecDataBuilder();
