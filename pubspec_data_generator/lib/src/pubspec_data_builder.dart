// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of pubspec_data.
//
// Pubspec_data is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pubspec_data is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with pubspec_data.  If not, see <https://www.gnu.org/licenses/>.

import 'package:build/build.dart';
import 'package:code_builder/code_builder.dart';
import 'package:dart_style/dart_style.dart';
import 'package:recase/recase.dart';
import 'package:yaml/yaml.dart';

class PubspecDataBuilder implements Builder {
  @override
  final buildExtensions = const {
    '.yaml': ['/../lib/src/pubspec.g.dart']
  };

  @override
  Future<void> build(BuildStep buildStep) async {
    final inputId = buildStep.inputId;
    final outputId = AssetId.parse('${inputId.package}|lib/src/pubspec.g.dart');

    final yaml = await buildStep.readAsString(inputId);
    final pubspec = loadYaml(yaml);

    final library = Library((l) => l
      ..body.addAll([
        _createStringField(
          'name',
          'Name of the package. Required.',
          pubspec,
        ),
        _createVersionField(
          'version',
          'Version of the package. Equals to [Version.none] if not set.',
          pubspec,
        ),
        _createStringField(
          'description',
          'Description of the package. Equals to [null] if not set.',
          pubspec,
        ),
        _createUriField(
          'homepage',
          'Homepage of the package. Equals to [null] if not set.',
          pubspec,
        ),
        _createUriField(
          'repository',
          'Repository of the package. Equals to [null] if not set.',
          pubspec,
        ),
        _createUriField(
          'issue_tracker',
          'Issue tracker of the package. Equals to [null] if not set.',
          pubspec,
        ),
        _createUriField(
          'documentation',
          'Documentation of the package. Equals to [null] if not set.',
          pubspec,
        ),
      ]));

    final emitter = DartEmitter.scoped();
    final formatter = DartFormatter();
    final code = formatter.format('${library.accept(emitter)}');

    await buildStep.writeAsString(outputId, code);
  }

  Field _createStringField(String name, String comment, dynamic pubspec) {
    return Field((f) => f
      ..docs.add('/// $comment')
      ..name = name.camelCase
      ..assignment = literal(pubspec[name]).code
      ..type = refer('String')
      ..modifier = FieldModifier.final$);
  }

  Field _createUriField(String name, String comment, dynamic pubspec) {
    final value = pubspec[name];
    final expression = value == null
        ? literal(null)
        : refer('Uri.parse').call([literal(value)]);

    return Field((f) => f
      ..docs.add('/// $comment')
      ..name = name.camelCase
      ..assignment = expression.code
      ..type = refer('Uri')
      ..modifier = FieldModifier.final$);
  }

  Field _createVersionField(String name, String comment, dynamic pubspec) {
    const package = 'package:pub_semver/pub_semver.dart';

    final value = pubspec[name];
    final expression = value == null
        ? refer('Version.none', package)
        : refer('Version.parse', package).call([literal(value)]);

    return Field((f) => f
      ..docs.add('/// $comment')
      ..name = name.camelCase
      ..assignment = expression.code
      ..type = refer('Version', package)
      ..modifier = FieldModifier.final$);
  }
}
