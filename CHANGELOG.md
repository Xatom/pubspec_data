# 1.0.0+2

* Add dummy file for analyzer.

# 1.0.0+1

* Fix README.md and CHANGELOG.md on pub.dev.

# 1.0.0

* Initial release.
